/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

/**
 *
 * @author ruben
 */
import java.awt.BorderLayout;
import java.awt.Font;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import javax.swing.*;


public class Principal extends JFrame {


 static double tamañoDescarga=10516957;
 JLabel ldescargado,lvelocidad,limagen,ltxtimg;
 double descargado=0;
 double tiempo=0;
 Thread hilotiempo;
 double t=0;
 boolean sw=true;
 JPanel pdescargado,pvelocidad;
 
 public Principal(){
  
  ldescargado=new JLabel("");
  ldescargado.setFont(new Font("Verdana",Font.BOLD,15));
  lvelocidad=new JLabel("");
  lvelocidad.setFont(new Font("Verdana",Font.BOLD,15));
  //limagen=new JLabel("    ",new ImageIcon(this.getClass().getResource("../lib/imagen.png")),SwingConstants.LEFT);
//  limagen.setHorizontalAlignment(SwingConstants.LEFT);
 // limagen.setHorizontalTextPosition(SwingConstants.LEFT);
  ltxtimg=new JLabel("Test de Velocidad");
  
  Thread hiloDescargador=new Thread(new Runnable(){


   @Override
   public void run() {
    try{
    URL url = new URL("http://www.somosarquitectura.es/somosarquitectura_2.pdf");
    URLConnection urlCon = url.openConnection();
    InputStream is = urlCon.getInputStream();
    FileOutputStream fos = new FileOutputStream("Java Zone paquete.pdf");
    byte[] array = new byte[1024];
    int leido = is.read(array);
    while (leido > 0) {
     descargado+=leido;
     ldescargado.setText(descargado+" bytes de "+tamañoDescarga+" bytes");
     fos.write(array, 0, leido);
     leido = is.read(array);
    }
    sw=false;
    ldescargado.setText("Descarga Completa");
    is.close();
    fos.close();
    Thread.sleep(100);
    DecimalFormat df=new DecimalFormat("000.00");
    String v=df.format(((tamañoDescarga/tiempo)/1000.0));
    lvelocidad.setText("La velocidad de bajada es de: "+v+" Kbps");
    }catch(IOException ioe){
     ioe.printStackTrace();
    }catch (InterruptedException ie){
     ie.printStackTrace();
    }
    
   }
   
  });
  hiloDescargador.start();
  hilotiempo=new Thread(new Runnable(){


   @Override
   public void run() {
    try {
     Thread.sleep(4000);
     while(sw){
      t+=100;
      Thread.sleep(100);
     }
     tiempo=t/1000;
    } catch (InterruptedException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
    }
   }
   
  });
  hilotiempo.start();
  
  pdescargado=new JPanel();
  pdescargado.add(ldescargado);
  pvelocidad=new JPanel();
  pvelocidad.add(lvelocidad);
  JPanel paux=new JPanel();
  JPanel pimagen=new JPanel();
  pimagen.setLayout(new BoxLayout(pimagen,BoxLayout.Y_AXIS));
  pimagen.add(ltxtimg);
//  pimagen.add(limagen);
  paux.add(pimagen);
  add(pdescargado);
  add(paux,BorderLayout.NORTH);
  add(pvelocidad,BorderLayout.SOUTH);
  
 }
 
 
}