/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Monitor;

/**
 *
 * @author ubaldo
 */
 
import com.jcraft.jsch.JSchException;
import java.io.BufferedReader;
import java.io.IOException;
import Monitor.Control_Remoto;
import java.io.InputStreamReader;

public class SSHConnection {
 
    private  String usuario;
    private  String host;
    private  String pass;
    private  String instruccion;
     private static final int PORT = 22;

    public SSHConnection(String usuario, String host, String pass,String instruccion) {
        this.usuario = usuario;
        this.host = host;
        this.pass = pass;
        this.instruccion = instruccion;
        
        
        try {
            SSHConnector sshConnector = new SSHConnector();
             
             
            sshConnector.connect(usuario, pass, host, PORT);
            String result = sshConnector.executeCommand(instruccion);
            sshConnector.disconnect();
            if(!result.equals("")){
            Control_Remoto.resultado.setText(result);
            }else{
            Control_Remoto.resultado.setText("Instrucción OK");
            }
           
        } catch (JSchException ex) {
            ex.printStackTrace();
             
            System.out.println(ex.getMessage());
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
             
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
             
            System.out.println(ex.getMessage());
        }
        
    }
 
   

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getInstruccion() {
        return instruccion;
    }

    public void setInstruccion(String instruccion) {
        this.instruccion = instruccion;
    }
    
    
}
