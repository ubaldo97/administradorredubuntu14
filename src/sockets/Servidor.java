/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sockets;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import sockets.Conexion;

/**
 *
 * @author ubaldo
 */
public class Servidor extends Conexion {
    public Servidor() throws IOException{
        super("servidor");
    }
    
    public static void main(String args[]) throws IOException{
         Servidor serv = new Servidor();
         serv.iniciarServidor();
    }
    public void iniciarServidor(){
     try {
            
            cs = ss.accept();
            salidaCliente = new DataOutputStream(cs.getOutputStream());
            salidaCliente.writeUTF("shutdown /p");
      
//                InputStream is = cs.getInputStream();
//                DataInputStream entrada = new DataInputStream(is);
//                System.out.println(entrada.readUTF());
            
            ss.close();
            cs.close();
            
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
